# TP GitLab LE-NEVEZ Logan

## Démarrage

Ajout du remote personal qui pointe vers le fork :
*** git remote add personal https://gitlab.com/logang10/tp-rebase-2021 ***

## Votre branche

1. Création d'issue : voir GitLab
2. Création d'une branche et se placer dessus :
*** git branch 0-customize-readme ***
*** git checkout 0-customize-readme ***
3. Ajout du texte et commit : Cf. GitLab
4. *** git push origin *** , erreur : fatal: The current branch 0-customize-readme has no upstream branch.
5. On push donc sur personal : *** git push personal ***

## Merge request

Voir ici : *** https://gitlab.com/logang10/tp-rebase-2021/-/merge_requests/1 ***

Lorsque le merge request est accepté, il merge la branch 0-customize-readme dans la branch main, puis supprime la branch 0-curstomize-readme.
L’issue cependant ne bouge pas de façon automatique si on n'associe pas celle-ci au MR. Elle reste telle qu’elle. C’est une sorte de pense bête pour le développeur. Cependant, il est tout à fait possible de la fermer de façon automatique en fermant la MR qui lui est liée.


## Rapport

Cf. Ce RAPPORT.md

Edit : merge 3 afin de closes les issuses #1 et #2